<?php
$from = "info@cssvillage.org.au";
$to = "info@cssvillage.org.au";
$developer = "info@cssvillage.org.au";


$subject = "Simple test for mail function";
$message = "This is a test to check if php mail function sends out the email ". date('d/m/Y H:i:s') ;


$headers = array();
// To send HTML mail, the Content-type header must be set
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=iso-8859-1';

// Additional headers
$headers[] = 'To:' . $to;
$headers[] = 'From:' . $from;
$headers[] = 'Cc:' . $developer;


$headers = array(
    'From' => $from,
    'Reply-To' => $from,
    'X-Mailer' => 'PHP/' . phpversion()
);

if (mail($to, $subject, $message, implode("\r\n", $headers))) {
   echo("
      Message successfully sent! 
   ". date('d/m/Y H:i:s'));
} else {
   echo("
      Message delivery failed, see error_logs...
   ");
}
?>